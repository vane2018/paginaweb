require('dotenv').config()
const express = require('express')
var hbs = require('hbs');

const app = express();
const port = process.env.PORT;

console.log(process.env.PORT)

console.log("el puerto es:",port);

hbs.registerPartials(__dirname + '/views/partials', function (err) {});


app.set('view engine', 'hbs');
app.use(express.static('public'));

app.get('/',(req,res)=>{
    res.render('home',{
        name:'Jujuy',
        title:'Prestamos Perico'
    });
})

app.get('/generic', function (req, res) {
    res.render('generic',{
        name:'Jujuy',
        title:'Prestamos Perico'
    });
})

app.get('/elements', function (req, res) {
    res.render('elements',{
        name:'Jujuy',
        title:'Prestamos Perico'
    });
})


app.listen(port);